// connection
const conn = require('../config/db');

// required modules
const jwt = require('jsonwebtoken');

exports.isAuthenticated = function (req, res, next) {
    if (typeof req.headers.authorization !== "undefined") {
        let token = req.headers.authorization.split(" ")[1];
        jwt.verify(token, "express-demo", { algorithm: "HS256" }, (err, user) => {
            if (err) {
                res.status(500).json({ error: "Not Authorized" });
                throw new Error("Not Authorized");
            }

            let sql = "SELECT EXISTS(SELECT * FROM users WHERE username = ?) as same";
            conn.query(sql, user.username, (err, res) => {
                if (user.iss === "express_demo" && res[0].same) {
                    return next();
                }
            });

        });
    } else {
        res.status(500).json({ error: "Not Authorized"});
        throw new Error("Not Authorized");
    }
};