// connection
const conn = require('../config/db');

let sql;

let cart = function(task) {
    this.cart_id = task.cart_id;
    this.product_id = task.product_id;
    this.quantity = task.quantity;
};

// ADD TO CART
cart.addToCart = function(data, result)
{
    sql = "INSERT INTO cart_product SET ?";

    conn.query(sql, [data], function(err, res) {
        if (err) new Throw(err);

        result(err, res);
    });
};

// REMOVE PRODUCT FROM CART
cart.remove = function (id, result) {
    sql = "DELETE FROM cart_product WHERE id = ?";

    conn.query(sql, [id], function(err, res) {
       result(err, res)
    });
};

// REMOVE MULTIPLE PRODUCTS FROM CART
cart.removeMultiple = function(id, result)
{
    sql = "DELETE FROM cart_product WHERE (id) IN (?)";

    conn.query(sql, [id], function(err, res) {
        result(err, res);
    });
};

// UPDATE PRODUCT IN CART
cart.updateCart = function(id, data, result)
{
    sql = "UPDATE cart_product SET ? WHERE id = ?";

    conn.query(sql, [data, id], function(err, res) {
        result(err, res);
    });
};

// LIST OF CARTS
cart.listCarts = function (id, result)
{
    sql =
        "select `carts`.`id` AS `cart_id`,sum(`cart_product`.`quantity`) AS `quantity_total`,sum(`cart_product`.`quantity` * `products`.`price_sell`) AS `price_total` from ((`cart_product` left join `carts` on(`cart_product`.`cart_id` = `carts`.`id`)) left join `products` on(`cart_product`.`product_id` = `products`.`id`));" +
        "select `cart_product`.`id` AS `list_id`,`products`.`name` AS `name`,`products`.`price_sell` AS `price_sell`,`cart_product`.`quantity` AS `quantity`,`products`.`price_sell` * `cart_product`.`quantity` AS `price_total` from ((`cart_product` left join `carts` on(`carts`.`id` = `cart_product`.`cart_id`)) left join `products` on(`products`.`id` = `cart_product`.`product_id`)) where `carts`.`id` = ?;";

    conn.query(sql, [id, id], function(err, res) {
        result(err, res);
    });
};

module.exports = cart;

