// connection
const conn = require('../config/db');

let product = function (task) {
    this.name = task.name;
    this.price = parseInt(task.price);
    this.stock = task.stock;
    this.discount = task.discount;
    this.status = 'PUBLISH';
};

let sql;

// GET ALL
product.getAll = function (result) {
    sql = "SELECT * FROM products";

    conn.query(sql, function (err, res) {
        if (err) new Throw(err);

        result(err, res);
    });
};

// GET BY ID
product.getById = function (id, result) {
    sql = "SELECT * FROM products WHERE id = ?";

    conn.query(sql, [id], function (err, res) {
        if (err) new Throw(err);

        result(null, res);
    });
};

// CREATE
product.create = function (data, result) {
    sql = "INSERT INTO products SET ?";

    conn.query(sql, [data], function (err, res) {
        if (err) new Throw(err);

        result(err, res);
    });
};

// UPDATE
product.update = function (id, data, result) {
    sql = "UPDATE products SET ? WHERE id = ?";

    conn.query(sql, [data, id], function (err, res) {
        if (err) new Throw(err);

        result(null, res);
    });
};

// UPDATE IMAGE
product.updateImage = function (id, link_image, result) {
    sql = "UPDATE products SET image = ?, updated_at = ? WHERE id = ?";

    conn.query(sql, [link_image, new Date(), id], function (err, res) {
        if (err) new Throw(err);

        result(null, res);
    });
};

// DESTROY & DESTROY MULTIPLE
product.destroy = function (id, result) {
    sql = "DELETE FROM products WHERE id = ?";

    conn.query(sql, [id], function (err, res) {
        if (err) new Throw(err);

        result(null, res);
    });
};

product.destroyMultiple = function (id, result) {
    sql = "DELETE FROM products WHERE (id) IN (?)";

    conn.query(sql, [id], function (err, res) {
        if (err) new Throw(err);

        result(null, res);
    });
};

module.exports = product;
