// connection
const conn = require('../config/db');

// required modules
const bcrypt = require('bcrypt');

let user = function (task) {
    this.fullname = task.fullname;
    this.email = task.email;
    this.username = task.username;
    this.password = task.password;
    this.role = "ADMIN";
};

let sql;

user.save = function (data, result) {
    sql = "INSERT INTO users SET ?";

    conn.query(sql, [data], function (err, res) {
        if (err) new Throw(err);

        result(err, res);
    });
};

user.get = function (username, result) {
    sql = "SELECT * FROM users WHERE username = ?";

    conn.query(sql, [username], function (err, res) {
        if (err) new Throw(err);

        result(err, res[0]);
    });
};

module.exports = user;