// required models
const cart = require('../models/cartModel');

// ADD TO CART (product_id, quantity)
exports.addToCart = function (req, res) {
    let newCart = new cart(req.body);
    newCart.added_at = new Date();

    cart.addToCart(newCart, function (err) {
        if (err) {
            res.send(err)
        } else {
            res.status(200).send('it has added to cart');
        }
    });
};

// REMOVE FROM CART SINGLE & MULTIPLE
exports.remove = function (req, res) {

    cart.remove(req.params.id, function (err) {
        if (err) {
            res.send(err);
        } else {
            res.status(200).send('it has removed to cart');
        }
    });
};

exports.removeMultiple = function (req, res) {
    let data = [];

    for (let i = 0; i < req.body.id.length; i++) {
        data[i] = [req.body.id[i]];
    }

    cart.removeMultiple(data, function (err) {
        if (err) {
            res.send(err);
        } else {
            res.status(200).send('it has removed to cart');
        }
    });
};

// UPDATE CARTS
exports.updateCarts = function (req, res) {
    cart.updateCart(req.params.id, req.body, function (err, result) {
        if (err) {
            res.send(err);
        } else {
            res.status(200).send('Successfully to update');
        }
    });
};

// LIST CARTS
exports.listCarts = function (req, res) {
    cart.listCarts(req.params.id, function (err, result) {
        if (err) {
            res.send(err);
        } else {
            if (result[0][0].cart_id) {
                result[0][0]['lists'] = result[1];

                let data = {
                    cart: result[0][0]
                };

                res.send(data);
            } else {
                res.json({"message": "cart is empty. come on add something!"});
            }
        }
    });
};
