// required modules
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

// required models
const user = require('../models/userModel');

exports.register = function (req, res) {
    let newUser = new user(req.body);
    newUser.created_at = new Date();
    newUser.password = bcrypt.hashSync(newUser.password, 10);

    if (!newUser.fullname || !newUser.username || !newUser.email || !newUser.password) {
        res.status(400).send({message: newUser});
    } else {
        user.save(newUser, function (err) {
            if (err) new Throw(err);

            res.status(200).send({message: "Successfully add new product!"});
        });
    }
};

exports.accessToken = function (req, res) {
    let username = req.body.username;
    let password = req.body.password;

    user.get(username, function (err, result) {
        let data = result;

        if (err) res.send(err);

        if (!result) {
            res.status(400).send({message: "username no exist!", status: 400});
        } else {
            bcrypt.compare(password, data.password, function (err, same) {
                if (!same) {
                    res.status(400).send({message: "password invalid!", status: 400});
                }

                data = {
                    iss: "express_demo",
                    username: data.username,
                    role: data.role,
                    exp: Math.floor(Date.now() / 1000) + (60 * 60)
                };

                let token = jwt.sign(data, "express-demo", {algorithm: 'HS256'});
                res.send({token: token});
            });
        }
    });
};
