// required modules
const fs = require('fs');
const mime = require('mime');
const randomString = require('randomstring');

// required models
const product = require('../models/productModel');

let data;

// GET ALL
exports.getAll = function (req, res) {
    product.getAll(function (err, result) {
        if (err) {
            res.send(err);
        } else {
            data = {data: result};
            res.send(data);
        }
    });
};

// GET BY ID
exports.getById = function (req, res) {
    product.getById(req.params.id, function (err, result) {
        if (err) {
            res.send(err);
        } else {
            res.send(result);
        }
    });
};

// ADD PRODUCTS
exports.addProduct = function (req, res) {
    let newProduct = new product(req.body);
    newProduct.price_sell = newProduct.price - (newProduct.price * (newProduct.discount / 100));
    newProduct.created_at = new Date();

    product.create(newProduct, function (err) {
        if (err) {
            res.send(err);
        } else {
            res.status(200).send('Successfully add new product');
        }
    });
};

// UPDATE PRODUCTS
exports.update = function (req, res) {
    req.body.update_at = new Date();

    product.update(req.params.id, req.body, function (err) {
        if (err) {
            res.send(err);
        } else {
            res.status(200).send('Successfully update product');
        }
    });
};

// DESTROY & DESTROY MULTIPLE
exports.destroy = function (req, res) {
    product.destroy(req.params.id, function (err) {
        if (err) {
            res.send(err);
        } else {
            res.status(200).send('Successfully delete product');
        }
    });
};

exports.destroyMultiple = function (req, res) {
    let data = [];

    for (let i = 0; i < req.body.id.length; i++) {
        data[i] = [req.body.id[i]];
    }

    product.destroyMultiple(data, function (err) {
        if (err) {
            res.send(err);
        } else {
            res.status(200).send('Successfully delete few products');
        }
    });
};

// UPLOAD FORM-DATA BASE64
exports.uploadImage = function (req, res) {
    let data = [];
    let url = req.protocol + '://' + req.get('host') + '/images/products/';

    let upload = new Promise(function (resolve, reject) {
        for (let i = 0; i < req.files.length; i++) {
            let filename = randomString.generate() + "." + mime.getExtension(req.files[i].mimetype);

            fs.writeFileSync('./public/images/products/' + filename, req.files[i].buffer, 'utf8');
            let new_image = new Buffer.from(fs.readFileSync('./public/images/products/' + filename)).toString('base64'),
                new_url = url + filename;
            let base64 = 'data:' + req.files[i].mimetype + ';base64,' + new_image;

            data[i] = {
                "url": new_url,
                "data": base64
            };
        }

        data[0].id = req.params.id;
        resolve(data);
    });

    upload.then(function (result) {
        product.updateImage(result[0].id, result[0].url, function (err, result) {
            if (err) res.send(err);
        });

        res.json({image: result});
    });
};

exports.uploadImageBase64 = function (req, res) {
    let base64image = req.body.image;
    let decodedImage, imageBuffer, type, extension, fileName;
    let images;

    let upload = new Promise(function (resolve, reject) {
        let url = req.protocol + '://' + req.get('host') + '/images/products/';
        let matches = base64image.match(/^data:([A-Za-z-+\\/]+);base64,(.+)$/),
            response = {},
            data = [];

        response.type = matches[1];
        response.data = new Buffer.from(matches[2], 'base64');

        decodedImage = response;

        imageBuffer = decodedImage.data;
        type = decodedImage.type;

        extension = mime.getExtension(type);
        fileName = randomString.generate() + "." + extension;

        try {
            let newUrl = url + fileName;
            fs.writeFileSync('./public/images/products/' + fileName, imageBuffer, 'utf8');
            images = newUrl;

            data.push({"url": newUrl, "data":base64image, "id":req.params.id});
            resolve(data);
        } catch (e) {

        }
    });

    upload.then(function (result) {
        product.updateImage(result[0].id, result[0].url, function (err, result) {
            if (err) res.send(err);
        });

        res.json({image: result[0]});
    });
};

exports.hello = function (req, res) {
  res.send("hello");
};
