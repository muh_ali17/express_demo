const config = require('./config/app');
const {port} = config;

const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const multer = require('multer');

// node package modules cors support in express
const cors = require('cors');

const auth = require('./middleware/isAuthenticated');

// set destination or buffer file !
const storage = multer.memoryStorage({
    // # IF USING DISK_STORAGE ----------------------------------------------------------
    // destination: path.join(__dirname + '/public/images/products/'),
    // filename: function (req, file, callback) {
    //     callback(null, randomString.generate() + path.extname(file.original-name)); }
    // # ---------------------------------------------------------------------------------
});

let upload = multer({
    storage: storage
});

// set files routes !
const products = require('./routes/products');
const carts = require('./routes/carts');
const users = require('./routes/users');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use(cookieParser());

app.use(upload.array('image', 5));

app.use(express.static(path.join(__dirname, 'public')));

// cors now enabled
app.use(cors());

// routes is used !
app.use('/api/products', auth.isAuthenticated, products);
app.use('/api/carts', auth.isAuthenticated, carts);
app.use('/api', users);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

app.listen(port, function () {
    console.log(`You are running on port : ${port}`);
});

module.exports = app;
