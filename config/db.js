const config =  require('./app');

const mysql = require('mysql');

const {db_host, db_user, db_pass, db_name} = config;

const conn = mysql.createConnection({
    host: db_host,
    user: db_user,
    password: db_pass,
    database: db_name,
    multipleStatements: true
});

conn.connect();

module.exports = conn;

