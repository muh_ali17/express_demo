const dotenv = require('dotenv');
const result = dotenv.config();

const env = process.env.NODE_ENV || "development";
const config = require('../config/database')[env];

if (result.error) new Throw(result.error);

const {parsed: envs} = result;

/* server port */
envs.port = process.env.PORT || 3000;

/* database connection */
envs.db_host = config.hostname;
envs.db_user = config.username;
envs.db_pass = config.password;
envs.db_name = config.database;

module.exports = envs;