// required modules
const express = require('express');
const router = express.Router();

// required controllers
const user = require('../controllers/userController');

// REGISTER
router.post('/register', function (req, res) {
    user.register(req, res);
});

// LOGIN
router.post('/accessToken', function (req, res ) {
    user.accessToken(req, res);
});

module.exports = router;
