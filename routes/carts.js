// required modules
const express = require('express');
const router = express.Router();

// required controllers
const cart = require('../controllers/cartController');

// ADD TO CART
router.post('/', function (req, res, next) {
    cart.addToCart(req, res);
});

// REMOVE PRODUCT FROM CART
router.delete('/remove-list/:id', function (req, res, next) {
    cart.remove(req, res);
});

// REMOVE MULTIPLE PRODUCTS IN CART
router.delete('/remove-list-multiple', function (req, res, next) {
    cart.removeMultiple(req, res);
});

// GET LIST CART BY ID
router.get('/list/:id', function (req, res, next) {
    cart.listCarts(req, res);
});

// UPDATE LISTS CART
router.put('/list/:id', function (req, res, next) {
    cart.updateCarts(req, res);
});

module.exports = router;