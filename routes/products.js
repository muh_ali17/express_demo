// required modules
const express = require('express');
const router = express.Router();

// required controllers
const product = require('../controllers/productController');

// GET ALL
router.get('/', function (req, res, next) {
    product.getAll(req, res);
});

// GET BY ID
router.get('/:id', function (req, res, next) {
    product.getById(req, res);
});

// ADD
router.post('/', function (req, res, next) {
    product.addProduct(req, res);
});

// UPDATE
router.put('/:id', function (req, res, next) {
    product.update(req, res);
});

// DELETE & MULTIPLE DELETE
router.delete('/:id', function (req, res, next) {
    product.destroy(req, res);
});

router.delete('/delete-multiple', function (req, res, next) {
    product.destroyMultiple(req, res);
});

// UPLOAD IMAGE BY FORM_DATA & RAW
router.post('/upload-image/:id', function (req, res, next) {
    product.uploadImage(req, res);
});

router.post('/upload-image-base64/:id', function (req, res, next) {
    product.uploadImageBase64(req, res);
});

module.exports = router;