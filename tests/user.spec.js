const chai = require('chai');
const expect = require('chai').expect;
const chaiHttp = require('chai-http');
const app = require('../app');
const faker = require('faker');

chai.use(chaiHttp);

let token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9." +
    "eyJpc3MiOiJleHByZXNzX2RlbW8iLCJ1c2VybmFtZSI6ImFkbWluMjAyMCIsInJvbGUiOiJBRE1JTiIsImlhdCI6MTU4MDI3ODQxOH0." +
    "k9UsZHlqpNPSYnvwnMfmMrqY8Z_uAjxNhXn5hUeHITQ";

describe('API Testing - USERS', function () {
    describe('POST /api/register', function () {
        let data = {
            "fullname": faker.name.findName(),
            "email": faker.internet.email(),
            "username": faker.name.firstName() + Math.floor(Math.random() * 1000),
            "password": "123"
        };

        it('it should create new user return 200', function (done) {
            chai.request(app)
                .post('/api/register')
                .send(data)
                .end(function (err, res) {

                    expect(res.status).to.equal(200);
                    done()
                });
        });

        it("it shouldn't create new user", function (done) {
            data.fullname = null;

            chai.request(app)
                .post('/api/register')
                .send(data)
                .end(function (err, res) {

                    expect(res.status).to.equal(400);
                    done();
                });
        });

    });

    describe('POST /api/accessToken', function () {
        let data = {"username": "admin2020", "password": "admin2020"};

        it('it should return 200 & token', function (done) {
            chai.request(app)
                .post('/api/accessToken')
                .send(data)
                .end(function (err, res) {

                    expect(res.status).to.equal(200);
                    expect(res.body).to.have.property('token');
                    done();
                });
        });

        it('it should return 400', function (done) {
            chai.request(app)
                .post('/api/accessToken')
                .send({"username": "a", "password": "b"})
                .end(function (err, res) {

                    expect(res.status).to.equal(400);
                    done();
                });
        });
    });
});