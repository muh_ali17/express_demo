const chai = require('chai');
const expect = require('chai').expect;
const chaiHttp = require('chai-http');
const app = require('../app');

chai.use(chaiHttp);

let token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9." +
    "eyJpc3MiOiJleHByZXNzX2RlbW8iLCJ1c2VybmFtZSI6ImFkbWluMjAyMCIsInJvbGUiOiJBRE1JTiIsImlhdCI6MTU4MDI3ODQxOH0." +
    "k9UsZHlqpNPSYnvwnMfmMrqY8Z_uAjxNhXn5hUeHITQ";

describe('API Testing - PRODUCTS', function () {
    // let data = {
    //         "name": "Monitor Asus Rog 32 inch",
    //         "price": 20000000,
    //         "stock": 10,
    //         "discount": 5
    //     };
    //
    // describe('GET /api/products', function () {
    //     it('it should retrieve all data products', function (done) {
    //         chai.request(app)
    //             .get('/api/products')
    //             .set("Authorization", "Bearer " + token)
    //             .end(function (err, res) {
    //                 expect(res.status).to.equal(200);
    //                 expect(res.body).to.have.property('data');
    //                 done();
    //             });
    //     });
    // });
    //
    // describe('GET /api/products/:id', function () {
    //     it('it should retrieve specific data products', function () {
    //         chai.request(app)
    //             .get('/api/products/2')
    //             .set("Authorization", "Bearer " + token)
    //             .end(function (err, res) {
    //                 expect(res.status).to.equal(200);
    //             });
    //     });
    // });
    //
    // describe('POST /api/products', function () {
    //     it('it should add new product', function () {
    //         chai.request(app)
    //             .post('/api/products')
    //             .set("Authorization", "Bearer " + token)
    //             .send(data)
    //             .end(function (err, res) {
    //                 expect(res.status).to.equal(200);
    //             });
    //     });
    // });
    //
    // describe('PUT /api/products/:id', function () {
    //     it('it should update product', function () {
    //         chai.request(app)
    //             .put('/api/products')
    //             .set("Authorization", "Bearer " + token)
    //             .send({"id": 3, "name": "Laptop Asus Rog 32 inch"})
    //             .end(function (err, res) {
    //                 expect(res.status).to.equal(200);
    //             });
    //     });
    // });
    //
    // describe('DEL /api/products/:id', function () {
    //     it('it should delete product', function () {
    //         chai.request(app)
    //             .del('/api/products')
    //             .set("Authorization", "Bearer " + token)
    //             .send({"id": 4})
    //             .end(function (err, res) {
    //                 expect(res.status).to.equal(200);
    //             });
    //     });
    // });
    //
    // describe('DEL /api/products/delete-multiple', function () {
    //     it('it should delete few products in one time', function () {
    //         chai.request(app)
    //             .del('/api/products/delete-multiple')
    //             .set("Authorization", "Bearer " + token)
    //             .send({"id": [5, 6]})
    //             .end(function (err, res) {
    //                 expect(res.status).to.equal(200);
    //             });
    //     });
    // });
});