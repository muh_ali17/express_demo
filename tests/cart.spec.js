const chai = require('chai');
const expect = require('chai').expect;
const chaiHttp = require('chai-http');
const app = require('../app');

chai.use(chaiHttp);

let token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9." +
    "eyJpc3MiOiJleHByZXNzX2RlbW8iLCJ1c2VybmFtZSI6ImFkbWluMjAyMCIsInJvbGUiOiJBRE1JTiIsImlhdCI6MTU4MDI3ODQxOH0." +
    "k9UsZHlqpNPSYnvwnMfmMrqY8Z_uAjxNhXn5hUeHITQ";

describe('API Testing - CARTS', function () {

    describe('GET /api/carts/list/:id', function () {
        it('it should retrieve all data carts', function () {
            chai.request(app)
                .get('/api/carts/list/1')
                .set("Authorization", "Bearer " + token)
                .end(function (err, res) {
                    expect(res.status).to.equal(200);
                });
        });
    });

    describe('POST /api/carts', function () {
        it('it should add product to cart', function () {
            chai.request(app)
                .post('/api/carts')
                .set("Authorization", "Bearer " + token)
                .send({"cart_id":1, "product_id":10, "quantity":5})
                .end(function (err, res) {
                    expect(res.status).to.equal(200);
                });
        });
    });

    describe('PUT /api/carts/list/:id', function () {
        it('it should update product in cart', function () {
            chai.request(app)
                .put('/api/carts/list/12')
                .set("Authorization", "Bearer " + token)
                .send({"quantity":5})
                .end(function (err, res) {
                    expect(res.status).to.equal(200);
                });
        });
    });

    describe('DEL /api/carts/remove-list/:id', function () {
        it('it should delete product in cart', function () {
            chai.request(app)
                .del('/api/carts/remove-list/12')
                .set("Authorization", "Bearer " + token)
                .end(function (err, res) {
                    expect(res.status).to.equal(200);
                });
        });
    });

    describe('DEL /api/carts/remove-list-multiple', function () {
        it('it should delete few carts in cart one time', function () {
            chai.request(app)
                .del('/api/carts/remove-list-multiple')
                .set("Authorization", "Bearer " + token)
                .send({"id":[14, 15]})
                .end(function (err, res) {
                    expect(res.status).to.equal(200);
                });
        });
    });
});
