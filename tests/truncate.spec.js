const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = require('chai').expect;
const conn = require('../config/db');

chai.use(chaiHttp);

let sql;

describe("PRETEST", function () {
    it("DELETE ALL USERS", function (done) {
        sql = "DELETE FROM users; ALTER TABLE users AUTO_INCREMENT = 1;";

        conn.query(sql, function (err, res) {
            if (err) new Throw(err);

            expect(res[0].serverStatus).to.equal(2);
            expect(res[1].serverStatus).to.equal(2);
            done();
        });
    });

    it("DELETE ALL PRODUCTS", function(done) {
        sql = "DELETE FROM products; ALTER TABLE products AUTO_INCREMENT = 1;";

        conn.query(sql, function (err, res) {
            if (err) new Throw(err);

            expect(res[0].serverStatus).to.equal(2);
            expect(res[1].serverStatus).to.equal(2);
            done();
        });
    });

    it("DELETE ALL CARTS", function(done) {
        sql = "DELETE FROM carts; ALTER TABLE carts AUTO_INCREMENT = 1;";

        conn.query(sql, function (err, res) {
            if (err) new Throw(err);

            expect(res[0].serverStatus).to.equal(2);
            expect(res[1].serverStatus).to.equal(2);
            done();
        });
    });

    it("TRUNCATE CART_PRODUCT", function (done) {
        sql = "TRUNCATE TABLE cart_product";

        conn.query(sql, function (err, res) {
            if (err) new Throw(err);

            expect(res.serverStatus).to.equal(2);
            done()
        });
    });
});
